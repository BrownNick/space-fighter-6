
#include "Ship.h"


Ship::Ship()
{
	SetPosition(0, 0); //setting the position of the ship
	SetCollisionRadius(10); //seting collision radius 

	m_speed = 300; // max speed
	m_maxHitPoints = 3; //max hit points 
	m_isInvulnurable = false; //makes it able to take hits

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();// whem the weapon can shoot
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage) // when ship is hit 
{
	if (!m_isInvulnurable) // it becomes invulnurable
	{
		m_hitPoints -= damage; // - the damage 

		if (m_hitPoints <= 0) 
		{
			GameObject::Deactivate();// if it has more hit points it decativates invulnurable
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints; //the ship got hit with its max hit points
}

void Ship::FireWeapons(TriggerType type) //starts fire weapon
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}