
#include "EnemyShip.h"

//Setting the amount of hits an enemy ship can take 
EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{//setting the delay for when the enemy shows up
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)// EX: Pure Virtual
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);//setting the position of the enemy ship
	m_delaySeconds = delaySeconds;//delaying the ships

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{//when they take damage 
	Ship::Hit(damage);
}